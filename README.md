# B4U10S35-CQP2022-IDGEO

Vous allez réaliser tous ensemble un projet git.

Ce projet vous comptera comme membre et sera public. Il vous faut donc vous inscrire et créer un compte si ce n'est pas encore le cas.

Il s'agira de réaliser une étude exhaustive des différents types de licences que vous utilisez régulièrement, que ce soit les licences

logicielles, les licences sur les données ou sur les langages de programmation.

Afin de mener à bien cette étude, nous allons nous positionner en mode projet. Commençons par un brain-storming individuel

que nous mutualiserons dans un second temps. Cette mutualisation doit conduire :

- à la mise en place d'une méthodologie (wiki ou readme)

- à l'établissement d'un plan d'action (qui fait quoi)

- à une gestion des tâches sous forme de ticket

- envisager une licence adaptée à ce projet

L'objectif est de produire un projet diffusable (public) sur lequel on pourrait en faire une communication. Il nous servira aussi

de base documentaire dans un avenir proche ou lointain.
